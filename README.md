Activity: Resize Images with Command Line Tools
Install ImageMagick
Review the assigned reading: Save Time by Transforming Images in the Command Line, and install ImageMagick as directed (brew install imagemagick).

Creating Thumbnails
Practice using the ImageMagick command line tools to resize and compress images.

You'll need to download the '06-resize-images' folder from kenzieacademy_env(cloud9 environment) which is shared with you. Move the folder to your local workspace. Open this folder in VS Code. Open the index.html file. Figure out how to use the "convert" command to create thumbnails of the five photos as directed, and replace the text links in index.html with the thumbnail images.

Creating a favicon
A web page can be associated with a small representative icon, called a favicon. These are shown next to the pages title in a browser tab, as well as in the browser's history. You can create a favicon for any of your pages.

Run this command to create a favicon from dog2.jpg:

convert dog2.jpg -define icon:auto-resize favicon.ico
If you look at the resulting file in finder, you may notice that it is squashed horizontally. This happened because favicons are always square, but the source image we used was rectangular. Try the following command instead to shave off the sides from the source image:

convert dog2.jpg -shave 200x10 -define icon:auto-resize favicon.ico
Add this line to the <head> of index.html to use this favicon with the page:

<link  rel="shortcut icon"  href="favicon.ico">